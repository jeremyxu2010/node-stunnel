var Getopt = require('node-getopt');
var getopt = new Getopt([
  ['', 'stunnelserverport=ARG', 'stunnel server port'],
  ['h', 'help', 'display this help'],
  ['v', 'version', 'show version']
]).bindHelp();
getopt.on('help', function (argv, options) {
  getopt.showHelp();
  process.exit(0);
});
getopt.on('version', function (argv, options) {
  var pjson = require('./package.json');
  console.log(pjson.name + ': ' + pjson.version);
  process.exit(0);
});
var opt = getopt.parseSystem();

var stunnelserverport = opt.options.stunnelserverport || 8323;

var net         = require('net'),
  fs            = require('fs'),
  pathlib       = require('path'),
  util          = require('util'),
  HashTable     = require('./util/hashtable'),
  async         = require('async'),
  logger        = require('./util/log-factory').getLogger(pathlib.basename(__filename, '.js')),
  dh            = require('./util/dh'),
  Packet        = require('./util/packet'),
  PacketFraming = require('./util/packet-framing'),
  iputil        = require('./util/iputil'),
  config        = require('./config/stunnel-config'),
  common        = require('./util/common');



(function start() {

  var server = null;

  function restart() {
    if (config.retriedTimes >= config.maxRetries) {
      throw new Error('Max retries have been exceeded, I give up.');
    }
    config.retriedTimes += 1;
    setTimeout(start, config.retryTimeout);
  }

  function sendRACKCreateTCPServerPacket(conn, result, callback) {
    var p = new Packet(Packet.ACK_CREATE_TCP_SERVER);
    p.feedUInt8(result);
    conn.write(p.getBuffer(), function (err) {
      if (err) {
        callback(err);
        return;
      }
      callback(null);
    });
  }

  function sendLACKCreateTCPServerPacket(conn, targetServerId, callback) {
    var p = new Packet(Packet.ACK_CREATE_TCP_SERVER);
    p.feedUInt32BE(targetServerId);
    conn.write(p.getBuffer(), function (err) {
      if (err) {
        callback(err);
        return;
      }
      callback(null);
    });
  }

  function sendNewSocketPacket(conn, forwardport, forwardClientId, callback) {
    var p = new Packet(Packet.NEW_TCP_SOCKET);
    p.feedUInt32BE(forwardport * 256 * 256 + forwardClientId);
    conn.write(p.getBuffer(), function (err) {
      if (err) {
        callback(err);
        return;
      }
      callback(null);
    });
  }

  function sendACKNewSocketPacket(conn, callback) {
    var p = new Packet(Packet.ACK_NEW_TCP_SOCKET);
    conn.write(p.getBuffer(), function (err) {
      if (err) {
        callback(err);
        return;
      }
      callback(null);
    });
  }

  function createFowardServer(ctl_conn, forwardport, callback) {
    var forwardserver = net.createServer();
    forwardserver.params = {
      "nextForwardClientId" : 0,
      "in_conns"            : new HashTable()
    };
    forwardserver.on('listening', function () {
      callback(null, forwardserver);
    });
    forwardserver.on('connection', function (in_conn) {
      in_conn.setNoDelay(true);
      var forwardClientId = forwardserver.params.nextForwardClientId++;
      var pre_in_conn = forwardserver.params.in_conns.get(forwardClientId);
      if (pre_in_conn !== 'undefined') {
        forwardserver.params.in_conns.remove(forwardClientId);
        common.endQuietly(pre_in_conn);
      }
      in_conn.params = {
        "out_conn" : null,
        "paused": false
      };
      forwardserver.params.in_conns.put(forwardClientId, in_conn);
      logger.debug('rpipe in_conn need pause.');
      common.pauseConn(in_conn);
      in_conn.on('data', function (data) {
        var len = data.length;
        if (len > 0) {
          var packetMaxSize = config.packetMaxSize;
          var pkgNum = Math.floor((len + packetMaxSize - 1) / packetMaxSize);
          var writeCallback = function (err) {
            if (err) {
              common.endQuietly(in_conn.params.out_conn);
              return;
            }
            in_conn.params.out_conn.params.lastWriteTime = common.getCurrentTime();
          };
          var i;
          for (i = 0; i < pkgNum; i++) {
            var p = new Packet(Packet.DATA);
            if (i + 1 === pkgNum) {
              p.feedBuffer(data.slice(i * packetMaxSize, len));
            } else {
              p.feedBuffer(data.slice(i * packetMaxSize, (i + 1) * packetMaxSize));
            }
            var ret = in_conn.params.out_conn.write(p.getBuffer(), writeCallback);
            if (!ret) {
              logger.debug('rpipe in_conn need pause.');
              common.pauseConn(in_conn);
            }
          }
        }
      });
      in_conn.on('drain', function () {
        logger.debug('rpipe out_conn need resume.');
        common.resumeConn(in_conn.params.out_conn);
      });
      in_conn.on('error', function (err) {
        logger.debug('error in rpipe in_conn:', err);
        common.endQuietly(in_conn);
        return;
      });
      in_conn.on('close', function () {
        if (in_conn.params.out_conn !== null) {
          common.endQuietly(in_conn.params.out_conn);
          in_conn.params.out_conn = null;
        }
        forwardserver.params.in_conns.remove(forwardClientId);
      });
      sendNewSocketPacket(ctl_conn, forwardport, forwardClientId, function (err) {
        if (err) {
          common.endQuietly(ctl_conn);
          return;
        }
      });
    });
    forwardserver.on('error', function (err) {
      common.closeQuietly(forwardserver);
      logger.debug(err);
      callback(err);
    });
    forwardserver.on('close', function () {
      var forwardClientIds = forwardserver.params.in_conns.keys();
      forwardClientIds.forEach(function (e, i) {
        var conn = forwardserver.params.in_conns.get(e);
        common.endQuietly(conn);
      });
      forwardserver.params.in_conns.clear();
    });
    forwardserver.listen(forwardport);
  }

  function newLPipe(conn, targetip, targetport, callback) {
    var out_conn = net.createConnection(targetport, targetip);
    out_conn.setNoDelay(true);
    out_conn.params = {
      "in_conn": null,
      "paused": false
    };
    out_conn.on('connect', function () {
      logger.debug('lpipe out_conn connected to tcp server');
      callback(null, out_conn);
    });
    out_conn.on('data', function (data) {
      var len = data.length;
      if (len > 0) {
        var packetMaxSize = config.packetMaxSize;
        var pkgNum = Math.floor((len + packetMaxSize - 1) / packetMaxSize);
        var writeCallback = function (err) {
          if (err) {
            common.endQuietly(out_conn.params.in_conn);
            return;
          }
          out_conn.params.in_conn.params.lastWriteTime = common.getCurrentTime();
        };
        var i;
        for (i = 0; i < pkgNum; i++) {
          var p = new Packet(Packet.DATA);
          if (i + 1 === pkgNum) {
            p.feedBuffer(data.slice(i * packetMaxSize, len));
          } else {
            p.feedBuffer(data.slice(i * packetMaxSize, (i + 1) * packetMaxSize));
          }
          var ret = out_conn.params.in_conn.write(p.getBuffer(), writeCallback);
          if (!ret) {
            logger.debug('rpipe out_conn need pause.');
            common.pauseConn(out_conn);
          }
        }
      }
    });
    out_conn.on('error', function (err) {
      logger.debug('error in rpipe out_conn:', err);
      common.endQuietly(out_conn);
      return;
    });
    out_conn.on('drain', function () {
      logger.debug('rpipe in_conn need resume.');
      common.resumeConn(out_conn.params.in_conn);
    });
    out_conn.on('close', function () {
      if (out_conn.params.in_conn !== null) {
        common.endQuietly(out_conn.params.in_conn);
        out_conn.params.in_conn = null;
      }
      callback(new Error('lpipe out_conn closed.'));
    });
  }

  server = net.createServer();
  server.params = {
    "forwardServers" : new HashTable(),
    "nextTargetServerId" : 0,
    "targetServerInfos" : new HashTable()
  };
  server.on('listening', function () {
    config.retriedTimes = 0;
    logger.info("stunnel server listening on port " + stunnelserverport);
  });

  server.on('connection', function (conn) {
    conn.setNoDelay(true);
    conn.params = {
      "dhPriKey": null,
      "dhPubKey": null,
      "secretKey": null,
      "lastReadTime": -1,
      "lastWriteTime": -1,
      "sendHeartbeatTask": null,
      "checkHeartbeatTask": null,
      "packet_framing": new PacketFraming(),
      "paused": false,
      "serveControl": null,
      "mode": null,
      "forwardserver" : null,
      "forward_conn" : null,
      "targetServerId" : null
    };
    conn.params.packet_framing.on('frame', function (frame) {
      conn.params.lastReadTime = common.getCurrentTime();
      var contents = frame.contents;
      logger.debug('conn got a frame : [ type = ' + Packet.getPacketName(frame.type) + ', length = ' + contents.length + ']');
      var forwardport;
      var forwardInfo;
      var targetServerId;
      var targetip;
      var targetport;
      var forwardClientId;
      switch (frame.type) {
      case Packet.DH_KEY:
        async.waterfall([
          function (callback) {
            dh.DH_PublicKey_Foramt_Convert(contents, 'der', 'pem', callback);
          },
          function (peerPubKey, callback) {
            common.sendACKDHPacket(conn, callback);
          },
          function (callback) {
            conn.params.lastWriteTime = common.getCurrentTime();
            callback(null);
          }
        ], function (err) {
          if (err) {
            common.endQuietly(conn);
            return;
          }
        });
        break;
      case Packet.ACK_DH_KEY:
        conn.params.serveControl = true;
        async.waterfall([
          function (callback) {
            dh.DH_PublicKey_Foramt_Convert(contents, 'der', 'pem', function (err, peerPubKey) {
              callback(err, peerPubKey);
            });
          },
          function (peerPubKey, callback) {
            common.computeSecretKey(conn, peerPubKey, callback);
          },
          function (callback) {
            logger.debug('conn exchange dh key completed.');
            callback(null);
          }
        ], function (err) {
          if (err) {
            common.endQuietly(conn);
            return;
          }
        });
        break;
      case Packet.HEART_BEAT:
        common.sendACKHeartbeatPacket(conn, function (err) {
          if (err) {
            common.endQuietly(conn);
            return;
          }
          conn.params.lastWriteTime = common.getCurrentTime();
        });
        break;
      case Packet.ACK_HEART_BEAT:
        logger.debug('conn receive ack heartbeat packet.');
        break;
      case Packet.TUNNEL_MODE:
        if (conn.params.serveControl === true && (contents[0] === 0 || contents[0] === 1)) {
          if (contents.length > 0) {
            if (contents[0] === 0) {
              conn.params.mode = 'R';
            } else if (contents[0] === 1) {
              conn.params.mode = 'L';
            }
            common.sendACKModePacket(conn, 0, function (err) {
              if (err) {
                common.endQuietly(conn);
                return;
              }
            });
          } else {
            common.sendACKModePacket(conn, 1, function (err) {
              if (err) {
                common.endQuietly(conn);
                return;
              }
            });
          }
        }
        break;
      case Packet.CREATE_TCP_SERVER:
        if (conn.params.serveControl && conn.params.mode === 'R') {
          forwardport = contents.readUInt32BE(0);
          createFowardServer(conn, forwardport, function (err, forwardserver) {
            if (err) {
              sendRACKCreateTCPServerPacket(conn, 1, function (err) {
                common.endQuietly(conn);
                return;
              });
            } else {
              conn.params.forwardserver = forwardserver;
              server.params.forwardServers.put(forwardport, forwardserver);
              forwardserver.on('close', function () {
                common.endQuietly(conn);
              });
              sendRACKCreateTCPServerPacket(conn, 0, function (err) {
                if (err) {
                  common.endQuietly(conn);
                  return;
                }
              });
            }
          });
        } else if (conn.params.serveControl && conn.params.mode === 'L') {
          targetip = iputil.buffer_to_ipaddr(contents.slice(0, 4));
          targetport = contents.readUInt32BE(4);
          targetServerId = server.params.nextTargetServerId++;
          conn.params.targetServerId = targetServerId;
          server.params.targetServerInfos.put(targetServerId, {"targetip" : targetip, "targetport" : targetport});
          sendLACKCreateTCPServerPacket(conn, targetServerId, function (err) {
            if (err) {
              common.endQuietly(conn);
              return;
            }
          });
        }
        break;
      case Packet.ACK_NEW_TCP_SOCKET:
        if (conn.params.serveControl) {
          forwardInfo = contents.readUInt32BE(0);
          forwardport = Math.floor(forwardInfo / (256 * 256));
          forwardClientId = forwardInfo % (256 * 256);
          var forwardserver = server.params.forwardServers.get(forwardport);
          if (forwardserver === null) {
            common.endQuietly(conn);
            return;
          }
          var in_conn = forwardserver.params.in_conns.get(forwardClientId);
          if (in_conn === null) {
            common.endQuietly(conn);
            return;
          }
          in_conn.params.out_conn = conn;
          conn.params.forward_conn = in_conn;
          conn.params.serveControl = false;
          logger.debug('rpipe in_conn need resume.');
          common.resumeConn(in_conn);
        }
        break;
      case Packet.NEW_TCP_SOCKET:
        forwardInfo = contents.readUInt32BE(0);
        targetServerId = Math.floor(forwardInfo / (256 * 256));
        forwardClientId = forwardInfo % (256 * 256);
        var targetServerInfo = server.params.targetServerInfos.get(targetServerId);
        if (targetServerInfo !== null) {
          targetip = targetServerInfo.targetip;
          targetport = targetServerInfo.targetport;
          newLPipe(conn, targetip, targetport, function (err, out_conn) {
            if (err) {
              common.endQuietly(conn);
              return;
            }
            logger.debug('lpipe conn need pause.');
            common.pauseConn(conn);
            logger.debug('lpipe out_conn need pause.');
            common.pauseConn(out_conn);
            conn.params.forward_conn = out_conn;
            out_conn.params.in_conn = conn;
            sendACKNewSocketPacket(conn, function (err) {
              if (err) {
                common.endQuietly(conn);
              }
              conn.params.serveControl = false;
              logger.debug('lpipe conn need resume.');
              common.resumeConn(conn);
              logger.debug('lpipe out_conn need resume.');
              common.resumeConn(out_conn);
            });
          });
        } else {
          common.endQuietly(conn);
        }
        break;
      case Packet.CLOSE_TUNNEL:
        common.endQuietly(conn);
        break;
      case Packet.DATA:
        if (!conn.params.serveControl && conn.params.forward_conn !== null) {
          var ret = conn.params.forward_conn.write(contents, function (err) {
            if (err) {
              common.endQuietly(conn.params.forward_conn);
              return;
            }
          });
          if (!ret) {
            logger.debug('conn need pause.');
            common.pauseConn(conn);
          }
        } else {
          common.endQuietly(conn);
        }
        break;
      default:
        logger.error(new Error('conn invalid packet type.'));
        common.endQuietly(conn);
        break;
      }
    });
    conn.on('data', function (data) {
      conn.params.packet_framing.push(data, function () {
        common.pauseConn(conn);
      }, function () {
        common.resumeConn(conn);
      });
    });
    conn.on('drain', function () {
      logger.debug('forward_conn need resume.');
      common.resumeConn(conn.params.forward_conn);
    });
    conn.on('err', function (err) {
      common.endQuietly(conn);
      logger.error(err);
    });
    conn.on('close', function () {
      var forwardserver = conn.params.forwardserver;
      if (forwardserver !== null) {
        common.closeQuietly(forwardserver);
      }
      var targetServerId = conn.params.targetServerId;
      if (targetServerId !== null) {
        server.params.targetServerInfos.remove(targetServerId);
      }
      var forward_conn = conn.params.forward_conn;
      if (forward_conn !== null) {
        common.endQuietly(forward_conn);
      }
      if (config.heartbeat) {
        common.stopHeartbeatTask(conn);
      }
      conn.params.packet_framing.reset();
    });


    async.waterfall([
      function (callback) {
        if (config.heartbeat) {
          common.startHeartbeatTask(conn);
        }
        common.dh_init(conn, callback);
      },
      function (callback) {
        common.sendDHPacket(conn, callback);
      },
      function (callback) {
        conn.params.lastWriteTime = common.getCurrentTime();
        callback(null);
      }
    ], function (err) {
      if (err) {
        common.endQuietly(conn);
        return;
      }
    });

  });

  server.on('error', function (err) {
    common.closeQuietly(server);
    logger.error(err);
  });

  server.on('close', function () {
    logger.info('server got closed, will try to restart');
    restart();
  });

  server.listen(stunnelserverport);
}());