module.exports = {
  "retryTimeout" : 4000, // 2 seconds
  "retriedTimes" : 0,
  "maxRetries" : 5,
  "packetMaxSize" : 102400,
  "heartbeatInterval" : 2000,
  "heartbeatTimeout" : 12000,
  "heartbeat" : true,
  "maxReceiveBufferSize" : 6553600,
  "useFakeDHPubkey" : true,
  "fakeDHPubkey"  : "MIIBIzCBmQYJKoZIhvcNAQMBMIGLAoGBAPSI/VhOSdvNILSd5JEHNmszbDgNRR0PfIizHHxbLY7288kjwEPwpVsYjY67VYy4XTjTNP18F1dDox0YbN4zISy1Kv884bEpQBgRjXyEpwpy1obEAxnIByl6ypUM2Zafq9AKUJsCRtMIPWakXUGfnHy9iUsiGSa6q6Jew1XpL3jHAgECAgICAAOBhAACgYByBbUUYefDs+7nV3mquMwGizuyDKNbZtPTjglPe7XOhdgwOqYOj7oqDHJ6HVePd2FVMFFINo0EvmcjfOZdNrX0sneyc+2nmE1Jy9j0Z9teq5gKllE1CJwr4pvZOKW02Qi7IPVc/+HlLBnSyuwAElK6CCD39vOLu0MptC/YRtnOrw==",
  "logLevel" : "INFO"
};