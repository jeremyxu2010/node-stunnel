# node-stunnel

## Objective

stunnel server and client written by nodejs


## Usage
**Server**

```
node ./stunnel-server.js --stunnelserverport={stunnelserverport}
```

**Client**

*R mode, forward local's target port to stunnel server's port:*

```
node ./stunnel-client.js --mode=R --stunnelserverip={stunnelserverip} --stunnelserverport={stunnelserverport} --forwardport={forwardport} --targetip={targetip} --targetport={targetport}
```

*L mode, forward remote's target port to local's port:*

```
node ./stunnel-client.js --mode=L --stunnelserverip={stunnelserverip} --stunnelserverport={stunnelserverport} --forwardport={forwardport} --targetip={targetip} --targetport={targetport}
```
## Developing



### Tools
[Sublime Text](http://www.sublimetext.com/)