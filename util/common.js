var pathlib     = require('path'),
  dh            = require('./dh'),
  Packet        = require('./packet'),
  config        = require('../config/stunnel-config');

exports.dh_init = function (conn, callback) {
  if (config.useFakeDHPubkey) {
    callback(null);
  } else {
    dh.DH_generatePrivateKey(pathlib.join(__dirname, '../config/dhp.pem'),
      function (err, dhparam, priKey, pubKey) {
        if (err) {
          callback(err);
          return;
        }
        conn.params.dhPriKey = priKey;
        conn.params.dhPubKey = pubKey;
        callback(null);
      });
  }
};

exports.computeSecretKey = function (conn, peerPubKey, callback) {
  if (config.useFakeDHPubkey) {
    callback(null);
  } else {
    dh.DH_computeSecret(conn.params.dhPriKey, peerPubKey, function (err, data) {
      if (err) {
        callback(err);
        return;
      }
      conn.params.secretKey = data;
      callback(null);
    });
  }
};

exports.getCurrentTime = function () {
  return (new Date()).getTime();
};

exports.sendDHPacket = function (conn, callback) {
  if (config.useFakeDHPubkey) {
    var p = new Packet(Packet.DH_KEY);
    p.feedBuffer(new Buffer(config.fakeDHPubkey, 'base64'));
    conn.write(p.getBuffer(), function () {
      callback(null);
    });
  } else {
    dh.DH_PublicKey_Foramt_Convert(conn.params.dhPubKey, 'pem', 'der', function (err, derPubKey) {
      if (err) {
        callback(err);
        return;
      }
      var p = new Packet(Packet.DH_KEY);
      p.feedBuffer(derPubKey);
      conn.write(p.getBuffer(), function () {
        callback(null);
      });
    });
  }
};

exports.sendACKDHPacket = function (conn, callback) {
  if (config.useFakeDHPubkey) {
    var p = new Packet(Packet.ACK_DH_KEY);
    p.feedBuffer(new Buffer(config.fakeDHPubkey, 'base64'));
    conn.write(p.getBuffer(), function () {
      callback(null);
    });
  } else {
    dh.DH_PublicKey_Foramt_Convert(conn.params.dhPubKey, 'pem', 'der', function (err, derPubKey) {
      if (err) {
        callback(err);
        return;
      }
      var p = new Packet(Packet.ACK_DH_KEY);
      p.feedBuffer(derPubKey);
      conn.write(p.getBuffer(), function (err) {
        if (err) {
          callback(err);
          return;
        }
        callback(null);
      });
    });
  }
};

exports.sendACKHeartbeatPacket = function (conn, callback) {
  var p = new Packet(Packet.ACK_HEART_BEAT);
  conn.write(p.getBuffer(), function (err) {
    if (err) {
      callback(err);
      return;
    }
    callback(null);
  });
};

exports.sendHeartbeatPacket = function (conn, callback) {
  var p = new Packet(Packet.HEART_BEAT);
  conn.write(p.getBuffer(), function (err) {
    if (err) {
      callback(err);
      return;
    }
    callback(null);
  });
};

exports.sendModePacket = function (conn, mode, callback) {
  var p = new Packet(Packet.TUNNEL_MODE);
  p.feedUInt8(mode === 'R' ? 0 : 1);
  conn.write(p.getBuffer(), function (err) {
    if (err) {
      callback(err);
      return;
    }
    callback(null);
  });
};

exports.sendACKModePacket = function (conn, result, callback) {
  var p = new Packet(Packet.ACK_TUNNEL_MODE);
  p.feedUInt8(result);
  conn.write(p.getBuffer(), function (err) {
    if (err) {
      callback(err);
      return;
    }
    callback(null);
  });
};

exports.endQuietly = function (conn) {
  if (conn !== null && conn !== 'undefined') {
    try {
      conn.end();
    } catch (err) {}
  }
};

exports.closeQuietly = function (server) {
  if (server !== null && server !== 'undefined') {
    try {
      server.close();
    } catch (err) {}
  }
};

exports.startHeartbeatTask = function (conn) {
  var that = this;
  conn.params.lastWriteTime = this.getCurrentTime();
  conn.params.lastReadTime = this.getCurrentTime();
  conn.params.sendHeartbeatTask = setInterval(function () {
    var now = that.getCurrentTime();
    if ((now - conn.params.lastReadTime > config.heartbeatInterval) || (now - conn.params.lastWriteTime > config.heartbeatInterval)) {
      that.sendHeartbeatPacket(conn, function (err) {
        if (err) {
          that.endQuietly(conn);
          return;
        }
        conn.params.lastWriteTime = that.getCurrentTime();
      });
    }
  }, config.heartbeatInterval);
  conn.params.checkHeartbeatTask = setInterval(function () {
    var now = that.getCurrentTime();
    if ((now - conn.params.lastReadTime > config.heartbeatTimeout) || (now - conn.params.lastWriteTime > config.heartbeatTimeout)) {
      that.endQuietly(conn);
    }
  }, config.heartbeatInterval);
};

exports.stopHeartbeatTask = function (conn) {
  clearInterval(conn.params.sendHeartbeatTask);
  clearInterval(conn.params.checkHeartbeatTask);
  conn.params.sendHeartbeatTask = null;
  conn.params.checkHeartbeatTask = null;
  conn.params.lastWriteTime = -1;
  conn.params.lastReadTime = -1;
};

exports.pauseConn = function (conn) {
  if (!conn.params.paused) {
    conn.pause();
    conn.params.paused = true;
  }
};

exports.resumeConn = function (conn) {
  if (conn.params.paused) {
    conn.resume();
    conn.params.paused = false;
  }
};