var log4js   = require('log4js'),
  config     = require('../config/stunnel-config');

log4js.configure({
  appenders: [{
    type: 'console'
  }, {
    type : 'file',
    filename : 'logs/stunnel.log',
    maxLogSize : 10240,
    backups : 5
  }],
  replaceConsole: true
});

exports.getLogger = function (name) {
  var logger = log4js.getLogger(name);
  logger.setLevel(config.logLevel);
  return logger;
};