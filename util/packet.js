var BufferBuilder = require('buffer-builder');

function Packet(type) {
  this.headBuf = new Buffer(Packet.HEAD_LENGTH);
  this.headBuf.writeUInt8(type, 0);
  this.contentBuilder = new BufferBuilder();
}

Packet.prototype.feedBuffer = function (buffer) {
  this.contentBuilder.appendBuffer(buffer);
};

Packet.prototype.feedUInt8 = function (i) {
  this.contentBuilder.appendUInt8(i);
};

Packet.prototype.feedUInt32BE = function (i) {
  this.contentBuilder.appendUInt32BE(i);
};

Packet.prototype.getBuffer = function () {
  this.headBuf.writeInt32BE(this.contentBuilder.length, 1);
  var result = new BufferBuilder();
  result.appendBuffer(this.headBuf);
  result.appendBuffer(this.contentBuilder.get());
  return result.get();
};

Packet.HEAD_LENGTH = 5;
Packet.DH_KEY = 0x08;
Packet.ACK_DH_KEY = 0x09;
// 心跳
Packet.HEART_BEAT = 0x00;
// ACK心跳
Packet.ACK_HEART_BEAT = 0x01;
Packet.TUNNEL_MODE = 0x0a;
Packet.ACK_TUNNEL_MODE = 0x0b;
Packet.CREATE_TCP_SERVER = 0x02;
Packet.ACK_CREATE_TCP_SERVER = 0x03;
Packet.NEW_TCP_SOCKET = 0x04;
Packet.ACK_NEW_TCP_SOCKET = 0x05;
Packet.DATA = 0x06;
Packet.CLOSE_TUNNEL = 0x07;

Packet.getPacketName = function (value) {
  var result;
  switch (value) {
  case Packet.DH_KEY:
    result = "DH_KEY";
    break;
  case Packet.ACK_DH_KEY:
    result = "ACK_DH_KEY";
    break;
  case Packet.HEART_BEAT:
    result = "HEART_BEAT";
    break;
  case Packet.ACK_HEART_BEAT:
    result = "ACK_HEART_BEAT";
    break;
  case Packet.TUNNEL_MODE:
    result = "TUNNEL_MODE";
    break;
  case Packet.ACK_TUNNEL_MODE:
    result = "ACK_TUNNEL_MODE";
    break;
  case Packet.CREATE_TCP_SERVER:
    result = "CREATE_TCP_SERVER";
    break;
  case Packet.ACK_CREATE_TCP_SERVER:
    result = "ACK_CREATE_TCP_SERVER";
    break;
  case Packet.NEW_TCP_SOCKET:
    result = "NEW_TCP_SOCKET";
    break;
  case Packet.ACK_NEW_TCP_SOCKET:
    result = "ACK_NEW_TCP_SOCKET";
    break;
  case Packet.DATA:
    result = "DATA";
    break;
  case Packet.CLOSE_TUNNEL:
    result = "CLOSE_TUNNEL";
    break;
  default:
    result = "";
  }
  return result;
};

module.exports = Packet;