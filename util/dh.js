var spawn   = require("child_process").spawn,
  os        = require("os"),
  pathlib   = require("path"),
  fs        = require("fs"),
  crypto    = require("crypto"),
  tempDir   = os.tmpdir() || "/tmp",
  async     = require('async');

/**
 * Generically spawn openSSL, without processing the result
 *
 * @param {Array}
 *            params The parameters to pass to openssl
 * @param {String|Array}
 *            tmpfiles Stuff to pass to tmpfiles
 * @param {Function}
 *            callback Called with (error, exitCode, stdout, stderr)
 */
var spawnOpenCmd = function (command, params, callback) {
  var cmd = spawn(command, params), stdout = new Buffer(0), stderr = new Buffer(0);

  cmd.stdout.on('data', function (data) {
    stdout = Buffer.concat([ stdout, data ]);
  });

  cmd.stderr.on('data', function (data) {
    stderr = Buffer.concat([ stderr, data ]);
  });

  // We need both the return code and access to all of stdout. Stdout
  // isn't
  // *really* available until the close event fires; the timing nuance was
  // making this fail periodically.
  var needed = 2; // wait for both exit and close.
  var code = -1;
  var bothDone = function () {
    if (code) {
      callback(new Error("Invalid " + command + " exit code: " + code
          + "\n% " + command + " " + params.join(" ") + "\n"
          + stderr.toString('hex')), code);
    } else {
      callback(null, code, stdout, stderr);
    }
  };

  cmd.on('exit', function (ret) {
    code = ret;
    if (--needed < 1) {
      bothDone();
    }
  });

  cmd.on('close', function (ret) {
    if (--needed < 1) {
      bothDone();
    }
  });
};

var spawnWrapper = function (command, params, tmpfiles, callback) {
  var files = [];
  var paramFiles = null;

  if (tmpfiles) {
    tmpfiles = [].concat(tmpfiles || []);
    params.forEach(function (value, i) {
      if (value === "--TMPFILE--") {
        var fpath = pathlib.join(tempDir, crypto.randomBytes(20).toString("hex"));
        files.push({
          path : fpath,
          contents : tmpfiles.shift()
        });
        params[i] = fpath;
      }
    });
    paramFiles = files.slice();
  }

  var spawnCmd = function () {
    spawnOpenCmd(command, params, function (err, code, stdout, stderr) {
      paramFiles.forEach(function (file, i) {
        fs.unlink(file.path);
      });
      callback(err, code, stdout, stderr);
    });
  };

  var processFiles = function () {
    var file = files.shift();

    if (!file) {
      return spawnCmd();
    }

    fs.writeFile(file.path, file.contents, function (err, bytes) {
      processFiles();
    });
  };


  processFiles();

};

var generatePrivateKey = function (dhparam, callback) {
  var params = [ 'genpkey', '-paramfile', '--TMPFILE--', '-outform', 'pem' ];
  spawnWrapper('openssl', params, dhparam, function (err, code, stdout, stderr) {
    if (err) {
      callback(err, null);
      return;
    }
    callback(null, stdout);
  });
};

var getPublicKey = function (privateKey, formType, callback) {
  var params = [ 'pkey', '-in', '--TMPFILE--', '-outform', formType,
      '-pubout' ];
  spawnWrapper('openssl', params, privateKey, function (err, code, stdout, stderr) {
    if (err) {
      callback(err, null);
      return;
    }
    callback(null, stdout);
  });
};

exports.DH_generatePrivateKey = function (dhparamPath, callback) {
  fs.readFile(dhparamPath, function (err, dhparam) {
    if (err) {
      callback(err);
      return;
    }
    generatePrivateKey(dhparam, function (err, priKey) {
      if (err) {
        callback(err);
        return;
      }
      getPublicKey(priKey, 'pem', function (err, pubKey) {
        if (err) {
          callback(err);
          return;
        }
        callback(null, dhparam, priKey, pubKey);
      });
    });
  });
};

exports.DH_computeSecret = function (privateKey, other_public_key, callback) {
  var params = [ 'pkeyutl', '-derive', '-inkey', '--TMPFILE--', '-peerkey',
      '--TMPFILE--' ];
  spawnWrapper('openssl', params, [ privateKey, other_public_key ], function (err, code, stdout, stderr) {
    if (err) {
      callback(err, null);
      return;
    }
    callback(null, stdout);
  });
};

exports.DH_PublicKey_Foramt_Convert = function (pubKey, inform, outform, callback) {
  if (inform === 'der' && outform === 'pem') {
    var base64str = pubKey.toString('base64');
    var base64str_linebreak = "";
    var i;
    for (i = 0; 64 * i < base64str.length; i++) {
      var idx_start = 64 * i;
      var idx_end = (64 * (i + 1) < base64str.length ? 64 * (i + 1) : base64str.length);
      base64str_linebreak = base64str_linebreak.concat(base64str.slice(idx_start, idx_end), '\n');
    }
    var result = '-----BEGIN PUBLIC KEY-----'.concat('\n', base64str_linebreak, '-----END PUBLIC KEY-----', '\n');
    callback(null, result);
  } else if (inform === 'pem' && outform === 'der') {
    var start, end;
    pubKey = pubKey.toString('ascii');
    var startRegex = new RegExp("\\-+BEGIN PUBLIC KEY\\-+$", "m");
    start = pubKey.match(startRegex);
    if (start) {
      start = start.index + (start[0] || "").length;
    } else {
      start = -1;
    }
    var endRegex = new RegExp("^\\-+END PUBLIC KEY\\-+", "m");
    end = pubKey.match(endRegex);
    if (end) {
      end = end.index;
    } else {
      end = -1;
    }
    if (start >= 0 && end >= 0) {
      pubKey = pubKey.substring(start, end);
    }
    var buffer = new Buffer(pubKey, 'base64');
    callback(null, buffer);
  }
};