var ip = require('ip'),
  net = require('net');

exports.ipaddr_to_buffer = function (ipaddr) {
  if (!net.isIPv4(ipaddr)) {
    throw new Error('only support ipv6 address.');
  }
  return ip.toBuffer(ipaddr);
};

exports.buffer_to_ipaddr = function (buffer) {
  if (!Buffer.isBuffer(buffer) || buffer.length !== 4) {
    throw new Error('invalid ipaddr buffer');
  }
  return ip.toString(buffer);
};