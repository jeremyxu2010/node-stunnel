var BufferGroup   = require('buffer-group'),
  debug           = false,
  events          = require('events'),
  util            = require('util'),
  config          = require('../config/stunnel-config');

var ST_PARSE   = 1, // Parsing incoming data.  No full frame found yet.
  ST_EXTRACT   = 2; //

function PacketFraming() {
  this.header_length_size = 5;

  this.frame_header_reader = function (offset_buffer) {
    var type = offset_buffer.readInt8();
    var length = offset_buffer.readInt32BE();
    return {
      "type": type,
      "length": length
    };
  };

  this.current_frame_type = 0;

  this.current_frame_length = 0;

  this.current_frame_buffer = new BufferGroup();

  events.EventEmitter.call(this);
}
util.inherits(PacketFraming, events.EventEmitter);

PacketFraming.prototype.push = function (data_buffer, pausePushCallback, resumePushCallback) {
  if (!data_buffer || data_buffer.length === 0) {
    return;
  }

  var frame_type = this.current_frame_type,
    frame_length = this.current_frame_length,
    frame_buffer = this.current_frame_buffer,
    header_length_size = this.header_length_size,
    state = ST_PARSE,
    new_state = 0;

  // First, buffer the data
  frame_buffer.push(data_buffer);
  if (frame_buffer.length > config.maxReceiveBufferSize) {
    pausePushCallback();
  }
  while (state) {
    new_state = 0;
    // ST_PARSE phase where we ingest data and try to figure out where the
    // frame begins and ends.
    if (state === ST_PARSE) {
      if (frame_length  === 0) {
        // If we don't know the length of the next phase, first thing
        // is to buffer data until we have at least the data needed
        // to read the frame size.
        if (frame_buffer.length >= header_length_size) {
          var frame_header = this.frame_header_reader(frame_buffer.extract(header_length_size));
          frame_type = frame_header.type;
          frame_length = frame_header.length;
          new_state = ST_EXTRACT;
        }
      } else {
        //  We have the length of the frame, so we can progress to
        //  extracting the frame from the buffer.
        new_state = ST_EXTRACT;
      }
    } else {
      if (frame_buffer.length >= frame_length) {
        // We're in business!  We've got at least the data we need
        // for a frame (any maybe more). Our BufferGroup will take
        // care of the details of extracting just the bytes we need
        // for this frame and keep the rest intact.
        var frame_contents = frame_buffer.extract(frame_length);
        if (frame_buffer.length <= config.maxReceiveBufferSize) {
          resumePushCallback();
        }
        var full_frame = {
          "type" : frame_type,
          "length": frame_length,
          "contents": frame_contents.buf
        };
        // now we reset the frame state
        frame_length = 0;
        frame_type = 0;
        frame_contents = null;
        // and tell our listeners about the full frame.
        this.emit('frame', full_frame);
        full_frame = null;
        // If there is any bytes left, try to read another frame length
        // (and possibly) another full frame
        new_state = ST_PARSE;
      }
    }
    state = new_state;
  }
  this.current_frame_type = frame_type;
  this.current_frame_length = frame_length;
};

PacketFraming.prototype.reset = function () {
  this.current_frame_type = 0;
  this.current_frame_length = 0;
  this.current_frame_buffer.clear();
};

module.exports = PacketFraming;