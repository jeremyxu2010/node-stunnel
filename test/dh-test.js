var fs = require('fs'),
  pathlib = require('path'),
  dh = require('../util/dh'),
  async = require('async'),
  should = require('should');

/*global describe:true, it:true*/

describe('dh', function () {
  describe('#DH_generatePrivateKey()', function () {
    it('generate private key', function (done) {
      dh.DH_generatePrivateKey(pathlib.join(__dirname,
          '../config/dhp.pem'),
        function (err, dhparam, priKey, pubKey) {
          try {
            should.not.exist(err);
            should.exist(dhparam);
            should.exist(priKey);
            should.exist(pubKey);
          } catch (e) {
            return done(err);
          }
          done();
        });
    });
  });

  describe('#DH_computeSecret()', function () {
    it('computed secrets should be the same', function (done) {
      var generatePrivateKey = function (callback) {
        dh.DH_generatePrivateKey(pathlib.join(__dirname,
          '../config/dhp.pem'), function (err, dhparam, priKey,
          pubKey) {
          if (err) {
            return done(err);
          }
          callback(null, {
            "dhparam": dhparam,
            "priKey": priKey,
            "pubKey": pubKey
          });
        });
      };

      async.parallel([generatePrivateKey, generatePrivateKey],
        function (err, results) {
          if (err) {
            return done(err);
          }
          async.parallel([
            function (callback) {
              dh.DH_computeSecret(results[0].priKey,
                results[1].pubKey, callback);
            },
            function (callback) {
              dh.DH_computeSecret(results[1].priKey,
                results[0].pubKey, callback);
            }
          ], function (err, results) {
            if (err) {
              return done(err);
            }
            var secretKey1 = results[0].toString('base64');
            var secretKey2 = results[1].toString('base64');
            should.equal(secretKey1, secretKey2);
            done();
          });
        });
    });
  });

  describe('#DH_PublicKey_Foramt_Convert()', function () {
    it('dh public key format conversation', function (done) {
      var origPubKey = null;
      async.waterfall([
        function (callback) {
          dh.DH_generatePrivateKey(pathlib.join(__dirname, '../config/dhp.pem'), function (err, dhparam, priKey, pubKey) {
            if (err) {
              callback(err);
              return;
            }
            origPubKey = pubKey;
            callback(null, pubKey);
          });
        },
        function (pubKey, callback) {
          dh.DH_PublicKey_Foramt_Convert(pubKey, 'pem', 'der', function (err, derPubKey) {
            if (err) {
              callback(err);
              return;
            }
            callback(null, derPubKey);
          });

        },
        function (derPubKey, callback) {
          dh.DH_PublicKey_Foramt_Convert(derPubKey, 'der', 'pem', function (err, convertedPubKey) {
            if (err) {
              callback(err);
              return;
            }
            callback(null, convertedPubKey);
          });
        }
      ], function (err, convertedPubKey) {
        if (err) {
          done(err);
          return;
        }
        should.equal(convertedPubKey.toString('ascii'), origPubKey.toString('ascii'));
        done();
      });
    });
  });

});