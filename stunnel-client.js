#!/usr/bin/env node

var Getopt = require('node-getopt');
var getopt = new Getopt([
  ['', 'mode=ARG', 'tunnel mode [R | L]'],
  ['', 'stunnelserverip=ARG', 'stunnel server ip'],
  ['', 'stunnelserverport=ARG', 'stunnel server port'],
  ['', 'forwardport=ARG', 'forward port'],
  ['', 'targetip=ARG', 'target ip'],
  ['', 'targetport=ARG', 'target port'],
  ['h', 'help', 'display this help'],
  ['v', 'version', 'show version']
]).bindHelp();
getopt.on('help', function (argv, options) {
  getopt.showHelp();
  process.exit(0);
});
getopt.on('version', function (argv, options) {
  var pjson = require('./package.json');
  console.log(pjson.name + ': ' + pjson.version);
  process.exit(0);
});
var opt = getopt.parseSystem();

var mode            = opt.options.mode  || 'L',
  stunnelserverip   = opt.options.stunnelserverip || '127.0.0.1',
  stunnelserverport = opt.options.stunnelserverport || 8323,
  forwardport       = opt.options.forwardport || 8325,
  targetip          = opt.options.targetip || '127.0.0.1',
  targetport        = opt.options.targetport || 22;

var net         = require('net'),
  fs            = require('fs'),
  pathlib       = require('path'),
  util          = require('util'),
  HashTable     = require('./util/hashtable'),
  async         = require('async'),
  logger        = require('./util/log-factory').getLogger(pathlib.basename(__filename, '.js')),
  dh            = require('./util/dh'),
  Packet        = require('./util/packet'),
  PacketFraming = require('./util/packet-framing'),
  iputil        = require('./util/iputil'),
  config        = require('./config/stunnel-config'),
  common        = require('./util/common');

(function connect() {

  var ctl_conn    = null,
    modeAccepted  = false,
    ready         = false,
    out_conns     = new HashTable(),
    forwardserver = null;

  function reconnect() {
    if (config.retriedTimes >= config.maxRetries) {
      logger.error('Max retries have been exceeded, I give up.');
      logger.debug(new Error('Max retries have been exceeded, I give up.'));
      process.exit(-1);
    }
    config.retriedTimes += 1;
    setTimeout(connect, config.retryTimeout);
  }

  function sendRCreateTCPServerPacket(conn, forwardport, callback) {
    var p = new Packet(Packet.CREATE_TCP_SERVER);
    p.feedUInt32BE(forwardport);
    conn.write(p.getBuffer(), function (err) {
      if (err) {
        callback(err);
        return;
      }
      callback(null);
    });
  }

  function sendLCreateTCPServerPacket(conn, targetip, targetport, callback) {
    var p = new Packet(Packet.CREATE_TCP_SERVER);
    p.feedBuffer(iputil.ipaddr_to_buffer(targetip));
    p.feedUInt32BE(targetport);
    conn.write(p.getBuffer(), function (err) {
      if (err) {
        callback(err);
        return;
      }
      callback(null);
    });
  }

  function sendACKNewSocketPacket(conn, forwardport, forwardClientId, callback) {
    var p = new Packet(Packet.ACK_NEW_TCP_SOCKET);
    p.feedUInt32BE(forwardport * 256 * 256  + forwardClientId);
    conn.write(p.getBuffer(), function (err) {
      if (err) {
        callback(err);
        return;
      }
      callback(null);
    });
  }

  function sendNewSocketPacket(conn, targetServerId, forwardClientId, callback) {
    var p = new Packet(Packet.NEW_TCP_SOCKET);
    p.feedUInt32BE(targetServerId * 256 * 256 + forwardClientId);
    conn.write(p.getBuffer(), function (err) {
      if (err) {
        callback(err);
        return;
      }
      callback(null);
    });
  }

  function newRPipe(conn, forwardport, forwardClientId, callback) {
    var out_conn = out_conns.get(forwardClientId);
    if (out_conn !== 'undefined') {
      out_conns.remove(forwardClientId);
      common.endQuietly(out_conn);
    }
    out_conn = net.createConnection(targetport, targetip);
    out_conn.setNoDelay(true);
    out_conn.params = {
      "in_conn": null,
      "paused": false
    };
    out_conn.on('connect', function () {
      out_conns.put(forwardClientId, out_conn);
      logger.debug('rpipe out_conn connected to tcp server');
      logger.debug('rpipe out_conn need pause.');
      common.pauseConn(out_conn);
      var in_conn = net.createConnection(stunnelserverport, stunnelserverip);
      in_conn.setNoDelay(true);
      in_conn.params = {
        "dhPriKey": null,
        "dhPubKey": null,
        "secretKey": null,
        "lastReadTime": -1,
        "lastWriteTime": -1,
        "sendHeartbeatTask": null,
        "checkHeartbeatTask": null,
        "packet_framing": new PacketFraming(),
        "paused": false
      };
      in_conn.on('connect', function () {
        async.waterfall([
          function (callback) {
            logger.debug('rpipe in_conn connected to rserver');
            if (config.heartbeat) {
              common.startHeartbeatTask(in_conn);
            }
            common.dh_init(in_conn, callback);
          },
          function (callback) {
            common.sendDHPacket(in_conn, callback);
          },
          function (callback) {
            in_conn.params.lastWriteTime = common.getCurrentTime();
            callback(null);
          }
        ], function (err) {
          if (err) {
            common.endQuietly(in_conn);
            return;
          }
        });
      });
      in_conn.params.packet_framing.on('frame', function (frame) {
        in_conn.params.lastReadTime = common.getCurrentTime();
        var contents = frame.contents;
        logger.debug('rpipe in_conn got a frame : [ type = ' + Packet.getPacketName(frame.type) + ', length = ' + contents.length + ']');
        switch (frame.type) {
        case Packet.DH_KEY:
          async.waterfall([
            function (callback) {
              dh.DH_PublicKey_Foramt_Convert(contents, 'der', 'pem', callback);
            },
            function (peerPubKey, callback) {
              common.sendACKDHPacket(in_conn, callback);
            },
            function (callback) {
              in_conn.params.lastWriteTime = common.getCurrentTime();
              callback(null);
            }
          ], function (err) {
            if (err) {
              common.endQuietly(in_conn);
              return;
            }
          });
          break;
        case Packet.ACK_DH_KEY:
          async.waterfall([
            function (callback) {
              dh.DH_PublicKey_Foramt_Convert(contents, 'der', 'pem', function (err, peerPubKey) {
                callback(err, peerPubKey);
              });
            },
            function (peerPubKey, callback) {
              common.computeSecretKey(in_conn, peerPubKey, callback);
            },
            function (callback) {
              logger.debug('rpipe in_conn exchange dh key completed.');
              sendACKNewSocketPacket(in_conn, forwardport, forwardClientId, callback);
            },
            function (callback) {
              out_conn.params.in_conn = in_conn;
              in_conn.params.lastWriteTime = common.getCurrentTime();
              logger.debug('rpipe out_conn need resume.');
              common.resumeConn(out_conn);
              callback(null);
            }
          ], function (err) {
            if (err) {
              common.endQuietly(in_conn);
              return;
            }
          });
          break;
        case Packet.HEART_BEAT:
          common.sendACKHeartbeatPacket(in_conn, function (err) {
            if (err) {
              common.endQuietly(in_conn);
              return;
            }
            in_conn.params.lastWriteTime = common.getCurrentTime();
          });
          break;
        case Packet.ACK_HEART_BEAT:
          logger.debug('rpipe in_conn receive ack heartbeat packet.');
          break;
        case Packet.DATA:
          var ret = out_conn.write(contents, function (err) {
            if (err) {
              common.endQuietly(out_conn);
              return;
            }
          });
          if (!ret) {
            logger.debug('rpipe in_conn need pause.');
            common.pauseConn(in_conn);
          }
          break;
        default:
          logger.error(new Error('rpipe in_conn invalid packet type.'));
          common.endQuietly(in_conn);
          break;
        }
      });
      in_conn.on('data', function (data) {
        in_conn.params.packet_framing.push(data, function () {
          common.pauseConn(in_conn);
        }, function () {
          common.resumeConn(in_conn);
        });
      });
      in_conn.on('drain', function () {
        logger.debug('rpipe out_conn need resume.');
        common.resumeConn(out_conn);
      });
      in_conn.on('error', function (err) {
        logger.debug('error in rpipe in_conn:', err);
        common.endQuietly(in_conn);
      });
      in_conn.on('close', function () {
        if (config.heartbeat) {
          common.stopHeartbeatTask(in_conn);
        }
        in_conn.params.packet_framing.reset();
        common.endQuietly(out_conn);
      });
    });
    out_conn.on('data', function (data) {
      var len = data.length;
      if (len > 0) {
        var packetMaxSize = config.packetMaxSize;
        var pkgNum = Math.floor((len + packetMaxSize - 1) / packetMaxSize);
        var writeCallback = function (err) {
          if (err) {
            common.endQuietly(out_conn.params.in_conn);
            return;
          }
          out_conn.params.in_conn.params.lastWriteTime = common.getCurrentTime();
        };
        var i;
        for (i = 0; i < pkgNum; i++) {
          var p = new Packet(Packet.DATA);
          if (i + 1 === pkgNum) {
            p.feedBuffer(data.slice(i * packetMaxSize, len));
          } else {
            p.feedBuffer(data.slice(i * packetMaxSize, (i + 1) * packetMaxSize));
          }
          var ret = out_conn.params.in_conn.write(p.getBuffer(), writeCallback);
          if (!ret) {
            logger.debug('rpipe out_conn need pause.');
            common.pauseConn(out_conn);
          }
        }
      }
    });
    out_conn.on('error', function (err) {
      logger.debug('error in rpipe out_conn:', err);
      common.endQuietly(out_conn);
      return;
    });
    out_conn.on('drain', function () {
      logger.debug('rpipe in_conn need resume.');
      common.resumeConn(out_conn.params.in_conn);
    });
    out_conn.on('close', function () {
      if (out_conn.params.in_conn !== null) {
        common.endQuietly(out_conn.params.in_conn);
        out_conn.params.in_conn = null;
      }
      out_conns.remove(forwardClientId);
    });
  }

  function createForwardServer(targetServerId, forwardport, callback) {
    var server = net.createServer();
    server.params = {
      "nextForwardClientId" : 0,
      "in_conns"            : new HashTable()
    };
    server.on('listening', function () {
      callback(null, server);
    });
    server.on('connection', function (in_conn) {
      in_conn.setNoDelay(true);
      var forwardClientId = server.params.nextForwardClientId++;
      var pre_in_conn = server.params.in_conns.get(forwardClientId);
      if (pre_in_conn !== 'undefined') {
        server.params.in_conns.remove(forwardClientId);
        common.endQuietly(pre_in_conn);
      }
      in_conn.params = {
        "out_conn" : null,
        "paused": false
      };
      server.params.in_conns.put(forwardClientId, in_conn);
      logger.debug('lpipe in_conn need pause.');
      common.pauseConn(in_conn);
      var out_conn = net.createConnection(stunnelserverport, stunnelserverip);
      out_conn.setNoDelay(true);
      out_conn.params = {
        "dhPriKey": null,
        "dhPubKey": null,
        "secretKey": null,
        "lastReadTime": -1,
        "lastWriteTime": -1,
        "sendHeartbeatTask": null,
        "checkHeartbeatTask": null,
        "packet_framing": new PacketFraming(),
        "paused": false
      };
      out_conn.on('connect', function () {
        async.waterfall([
          function (callback) {
            logger.debug('lpipe out_conn connected to rserver');
            if (config.heartbeat) {
              common.startHeartbeatTask(out_conn);
            }
            common.dh_init(out_conn, callback);
          },
          function (callback) {
            common.sendDHPacket(out_conn, callback);
          },
          function (callback) {
            out_conn.params.lastWriteTime = common.getCurrentTime();
            callback(null);
          }
        ], function (err) {
          if (err) {
            common.endQuietly(out_conn);
            return;
          }
        });
      });
      out_conn.params.packet_framing.on('frame', function (frame) {
        out_conn.params.lastReadTime = common.getCurrentTime();
        var contents = frame.contents;
        logger.debug('lpipe out_conn got a frame : [ type = ' + Packet.getPacketName(frame.type) + ', length = ' + contents.length + ']');
        switch (frame.type) {
        case Packet.DH_KEY:
          async.waterfall([
            function (callback) {
              dh.DH_PublicKey_Foramt_Convert(contents, 'der', 'pem', callback);
            },
            function (peerPubKey, callback) {
              common.sendACKDHPacket(out_conn, callback);
            },
            function (callback) {
              out_conn.params.lastWriteTime = common.getCurrentTime();
              callback(null);
            }
          ], function (err) {
            if (err) {
              common.endQuietly(out_conn);
              return;
            }
          });
          break;
        case Packet.ACK_DH_KEY:
          async.waterfall([
            function (callback) {
              dh.DH_PublicKey_Foramt_Convert(contents, 'der', 'pem', callback);
            },
            function (peerPubKey, callback) {
              common.computeSecretKey(out_conn, peerPubKey, callback);
            },
            function (callback) {
              logger.debug('lpipe out_conn exchange dh key completed.');
              sendNewSocketPacket(out_conn, targetServerId, forwardClientId, callback);
            },
            function (callback) {
              out_conn.params.lastWriteTime = common.getCurrentTime();
              callback(null);
            }
          ], function (err) {
            if (err) {
              common.endQuietly(out_conn);
              return;
            }
          });
          break;
        case Packet.HEART_BEAT:
          common.sendACKHeartbeatPacket(out_conn, function (err) {
            if (err) {
              common.endQuietly(out_conn);
              return;
            }
            out_conn.params.lastWriteTime = common.getCurrentTime();
          });
          break;
        case Packet.ACK_HEART_BEAT:
          logger.debug('lpipe out_conn receive ack heartbeat packet.');
          break;
        case Packet.ACK_NEW_TCP_SOCKET:
          in_conn.params.out_conn = out_conn;
          logger.debug('lpipe in_conn need resume.');
          common.resumeConn(in_conn);
          break;
        case Packet.DATA:
          var ret = in_conn.write(contents, function (err) {
            if (err) {
              common.endQuietly(in_conn);
              return;
            }
          });
          if (!ret) {
            logger.debug('lpipe out_conn need pause.');
            common.pauseConn(out_conn);
          }
          break;
        default:
          logger.error(new Error('lpipe out_conn invalid packet type.'));
          common.endQuietly(out_conn);
          break;
        }
      });
      out_conn.on('data', function (data) {
        out_conn.params.packet_framing.push(data, function () {
          common.pauseConn(out_conn);
        }, function () {
          common.resumeConn(out_conn);
        });
      });
      out_conn.on('drain', function () {
        logger.debug('lpipe in_conn need resume.');
        common.resumeConn(in_conn);
      });
      out_conn.on('error', function (err) {
        logger.debug('error in lpipe out_conn:', err);
        common.endQuietly(out_conn);
      });
      out_conn.on('close', function () {
        if (config.heartbeat) {
          common.stopHeartbeatTask(out_conn);
        }
        out_conn.params.packet_framing.reset();
        common.endQuietly(in_conn);
      });
      in_conn.on('data', function (data) {
        var len = data.length;
        if (len > 0) {
          var packetMaxSize = config.packetMaxSize;
          var pkgNum = Math.floor((len + packetMaxSize - 1) / packetMaxSize);
          var writeCallback = function (err) {
            if (err) {
              common.endQuietly(in_conn.params.out_conn);
              return;
            }
            in_conn.params.out_conn.params.lastWriteTime = common.getCurrentTime();
          };
          var i;
          for (i = 0; i < pkgNum; i++) {
            var p = new Packet(Packet.DATA);
            if (i + 1 === pkgNum) {
              p.feedBuffer(data.slice(i * packetMaxSize, len));
            } else {
              p.feedBuffer(data.slice(i * packetMaxSize, (i + 1) * packetMaxSize));
            }
            var ret = in_conn.params.out_conn.write(p.getBuffer(), writeCallback);
            if (!ret) {
              logger.debug('lpipe in_conn need pause.');
              common.pauseConn(in_conn);
            }
          }
        }
      });
      in_conn.on('drain', function () {
        logger.debug('lpipe out_conn need resume.');
        common.resumeConn(in_conn.params.out_conn);
      });
      in_conn.on('error', function (err) {
        logger.debug('error in lpipe in_conn:', err);
        common.endQuietly(in_conn);
        return;
      });
      in_conn.on('close', function () {
        if (in_conn.params.out_conn !== null) {
          common.endQuietly(in_conn.params.out_conn);
          in_conn.params.out_conn = null;
        }
        server.params.in_conns.remove(forwardClientId);
      });
    });
    server.on('error', function (err) {
      common.closeQuietly(server);
      logger.error(err);
      callback(err);
    });
    server.on('close', function () {
      var forwardClientIds = server.params.in_conns.keys();
      forwardClientIds.forEach(function (e, i) {
        var conn = server.params.in_conns.get(e);
        common.endQuietly(conn);
      });
      server.params.in_conns.clear();
    });
    server.listen(forwardport);
  }

  ctl_conn = net.createConnection(stunnelserverport, stunnelserverip);
  ctl_conn.setNoDelay(true);
  ctl_conn.params = {
    "dhPriKey": null,
    "dhPubKey": null,
    "secretKey": null,
    "lastReadTime": -1,
    "lastWriteTime": -1,
    "sendHeartbeatTask": null,
    "checkHeartbeatTask": null,
    "packet_framing": new PacketFraming(),
    "paused": false
  };

  ctl_conn.on('connect', function () {
    async.waterfall([
      function (callback) {
        config.retriedTimes = 0;
        logger.debug('ctl_conn connected to rserver');
        if (config.heartbeat) {
          common.startHeartbeatTask(ctl_conn);
        }
        common.dh_init(ctl_conn, callback);
      },
      function (callback) {
        common.sendDHPacket(ctl_conn, callback);
      },
      function (callback) {
        ctl_conn.params.lastWriteTime = common.getCurrentTime();
        callback(null);
      }
    ], function (err) {
      if (err) {
        common.endQuietly(ctl_conn);
        return;
      }
    });
  });

  ctl_conn.params.packet_framing.on('frame', function (frame) {
    ctl_conn.params.lastReadTime = common.getCurrentTime();
    var contents = frame.contents;
    logger.debug('ctl_conn got a frame : [ type = ' + Packet.getPacketName(frame.type) + ', length = ' + contents.length + ']');
    var error;
    switch (frame.type) {
    case Packet.DH_KEY:
      async.waterfall([
        function (callback) {
          dh.DH_PublicKey_Foramt_Convert(contents, 'der', 'pem', callback);
        },
        function (peerPubKey, callback) {
          common.sendACKDHPacket(ctl_conn, callback);
        },
        function (callback) {
          ctl_conn.params.lastWriteTime = common.getCurrentTime();
          callback(null);
        }
      ], function (err) {
        if (err) {
          common.endQuietly(ctl_conn);
          return;
        }
      });
      break;
    case Packet.ACK_DH_KEY:
      async.waterfall([
        function (callback) {
          dh.DH_PublicKey_Foramt_Convert(contents, 'der', 'pem', callback);
        },
        function (peerPubKey, callback) {
          common.computeSecretKey(ctl_conn, peerPubKey, callback);
        },
        function (callback) {
          logger.debug('ctl_conn exchange dh key completed.');
          common.sendModePacket(ctl_conn, mode, callback);
        },
        function (callback) {
          ctl_conn.params.lastWriteTime = common.getCurrentTime();
          callback(null);
        }
      ], function (err) {
        if (err) {
          common.endQuietly(ctl_conn);
          return;
        }
      });
      break;
    case Packet.HEART_BEAT:
      common.sendACKHeartbeatPacket(ctl_conn, function (err) {
        if (err) {
          common.endQuietly(ctl_conn);
          return;
        }
        ctl_conn.params.lastWriteTime = common.getCurrentTime();
      });
      break;
    case Packet.ACK_HEART_BEAT:
      logger.debug('ctl_conn receive ack heartbeat packet.');
      break;
    case Packet.ACK_TUNNEL_MODE:
      if (contents.length > 0 && contents[0] === 0) {
        modeAccepted = true;
        if (mode === 'R') {
          sendRCreateTCPServerPacket(ctl_conn, forwardport, function (err) {
            if (err) {
              common.endQuietly(ctl_conn);
              return;
            }
            ctl_conn.params.lastWriteTime = common.getCurrentTime();
          });
        } else if (mode === 'L') {
          sendLCreateTCPServerPacket(ctl_conn, targetip, targetport, function (err) {
            if (err) {
              common.endQuietly(ctl_conn);
              return;
            }
            ctl_conn.params.lastWriteTime = common.getCurrentTime();
          });
        }
      } else {
        logger.error(new Error('rserver deny this mode : ' + mode));
        common.endQuietly(ctl_conn);
      }
      break;
    case Packet.ACK_CREATE_TCP_SERVER:
      if (modeAccepted) {
        if (mode === 'R') {
          if (contents.length > 0 && contents[0] === 0) {
            logger.info('tunnel to rserver is ready.');
            ready = true;
          } else {
            logger.error('the tunnel with transit server is broken, or the forwardport on rtunnel server is already in use.');
            common.endQuietly(ctl_conn);
            return;
          }
        } else if (mode === 'L') {
          var targetServerId = contents.readUInt32BE(0);
          createForwardServer(targetServerId, forwardport, function (err, server) {
            if (err) {
              logger.error(new Error('error on create forward server on port' + forwardport));
              common.endQuietly(ctl_conn);
              return;
            }
            forwardserver = server;
            forwardserver.on('close', function () {
              common.endQuietly(ctl_conn);
              return;
            });
            logger.info('tunnel to rserver is ready.');
            ready = true;
          });
        }
      } else {
        error = new Error('tunnel mode is not accepted.');
        logger.error(error);
        throw error;
      }
      break;
    case Packet.NEW_TCP_SOCKET:
      if (ready) {
        var forwardInfo = contents.readUInt32BE(0);
        var temp = Math.floor(forwardInfo / (256 * 256));
        var forwardClientId = forwardInfo % (256 * 256);
        newRPipe(ctl_conn, forwardport, forwardClientId, function (err) {
          if (err) {
            return;
          }
          logger.debug('rpipe tunnel is ok.');
        });
      } else {
        error = new Error('tunnel is not ready');
        logger.error(error);
        throw error;
      }
      break;
    default:
      logger.error(new Error('ctl_conn invalid packet type.'));
      common.endQuietly(ctl_conn);
      break;
    }
  });

  ctl_conn.on('data', function (data) {
    ctl_conn.params.packet_framing.push(data, function () {
      common.pauseConn(ctl_conn);
    }, function () {
      common.resumeConn(ctl_conn);
    });
  });
  ctl_conn.on('error', function (err) {
    logger.error('error in ctl_conn:', err);
    common.endQuietly(ctl_conn);
  });
  ctl_conn.on('close', function () {
    logger.info('ctl_conn got closed, will try to reconnect');
    if (config.heartbeat) {
      common.stopHeartbeatTask(ctl_conn);
    }
    ctl_conn.params.packet_framing.reset();
    modeAccepted = false;
    ready = false;
    var forwardClientIds = out_conns.keys();
    forwardClientIds.forEach(function (e, i) {
      var conn = out_conns.get(e);
      common.endQuietly(conn);
    });
    out_conns.clear();
    common.closeQuietly(forwardserver);
    reconnect();
  });
}());